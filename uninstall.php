 <?php // Get out!


// Make sure this is a legitimate uninstall request
if( ! defined( 'ABSPATH') or ! defined('WP_UNINSTALL_PLUGIN') or ! current_user_can( 'delete_plugins' ) )
	exit();



function delete_geocms_posts() {


			$posts = array();
			$count = 0;

			$args = array(
				'numberposts' => -1,
				'post_type' => 'geopost'
			);

			$posts = get_posts( $args );

			foreach ($posts as $post) {
				$count++;
				wp_delete_post( $post->ID, true );
			}
			//reset_postdata();
			wp_reset_query();
			
			return $count;
	
	}


	/**

	 * Drop Geo Mashup database tables.

	 * 

	 * @since 1.3

	 * @access public

	 */

	 function delete_gsx_data() {

		global $wpdb;

		if ($geocms_options['delete_data']):

			$tables = array( 'geo_mashup_administrative_names', 'geo_mashup_location_relationships', 'geo_mashup_locations' );

			foreach( $tables as $table ) {

				$wpdb->query( 'DROP TABLE IF EXISTS ' . $wpdb->prefix . $table );

			}
			endif;
		}




	//note in multisite looping through blogs to delete options on each blog does not scale. You'll just have to leave them.

	/*

	* Getting options groups

	*/

	function delete_options() {


			delete_option( 'geocms_options' );

			delete_option( 'geo_mashup_temp_kml_url' );

			delete_option( 'geo_mashup_db_version' );

			delete_option( 'geo_mashup_activation_log' );

			delete_option( 'geo_mashup_options' );

			delete_option( 'geo_locations' );

			/*delete_site_option( 'geo_mashup_temp_kml_url' );

			delete_site_option( 'geo_mashup_db_version' );

			delete_site_option( 'geo_mashup_activation_log' );

			delete_site_option( 'geo_mashup_options' );

			delete_site_option( 'geo_locations' );
			
			delete_site_option( 'geocms_options' );*/


	}

